package net.mycompany;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.TimeoutException;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Properties;

public class Consumer {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Enter topic name");
            return;
        }

        final String topicName = args[0];
        Properties properties = new Properties();

        properties.put("bootstrap.servers", "localhost:9092");
        properties.put("group.id", "test2");
        properties.put("enable.auto.commit", "true");
        properties.put("key.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");

        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(properties);

        consumer.subscribe(Collections.singletonList(topicName));

        System.out.println("Subscribed to topic: " + topicName);

        while (true) {
            try {
                System.out.println("Try connecting...");
                consumer.listTopics(Duration.of(5, ChronoUnit.SECONDS));
            } catch (TimeoutException e) {
                continue;
            } catch (Exception e) {
                System.err.println(e);
                return;
            }
            break;
        }

        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.of(100, ChronoUnit.MILLIS));

            for (var record: records) {
                System.out.printf("offset = %d, key = %s, value = %s\n", record.offset(), record.key(), record.value());
            }
        }
    }
}
